# Graylog Helm chart

## Thanks to KongZ

This chart depends on the [KongZ graylog chart](https://github.com/KongZ/charts/tree/main/charts/graylog).

It has been a playground for me to learn the basis of Kubernetes and Helm. Thanks to [Kongz](https://github.com/KongZ) for his work.

# Use

## Prepare the chart

Use the `init.sh` before building and installing the chart.
It will set the graylog admin password and build opensearch and mongdb URIs for opensearch to be able to connect.


``` sh
$ ./init.sh
Enter the namespace: YOUR_NAMESPACE_HERE
Enter the default root password (min. length 16) : YOUR_ROOT_PASSWORD
Chosse your ingress host for the graylog web: YOUR_HOSTNAME
```

*Note: The script uses the ./services/graylog/values.tmpl.yaml as a template and writes the definitive values file.* 

## Build and install

Once the values are set, you are able to build and install the charts.

``` sh
$ helm dep build
$ helm install -n NAMESPACE .
```


# Default values

By default, the graylog service will be exposed (Service `type: LoadBalancer`) on port 1514 UDP.
You can add more ports by modifying the `input` in the graylog values (*./services/graylog/values.yaml*).

```yaml
input:
  tcp:
    service:
      type: LoadBalancer
      externalTrafficPolicy: Local
      loadBalancerIP:
    ports:
      - name: gelf1
        port: 12222
      - name: gelf2
        port: 12223
  udp:
    service:
      type: ClusterIP
    ports:
      - name: syslog
        port: 5410
```




