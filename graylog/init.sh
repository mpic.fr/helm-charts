#!/usr/bin/env sh

cp services/graylog/values.yaml.tmpl services/graylog/values.yaml

echo -n "Enter the namespace: "
read ns
sed -i -e "s/NAMESPACE/$ns/g" services/graylog/values.yaml

echo -n "Enter the default root password (min. length 16): "
read pwd
sed -i -e "s/ROOT_PASSWORD/$pwd/g" services/graylog/values.yaml

echo -n "Chosse your ingress host for the graylog web: "
read ingress_host
sed -i -e "s/INGRESS_HOST/$ingress_host/g" services/graylog/values.yaml

echo "Done ! You can build and install the chart."
